# Basic Exoplayer app

## Description

Android Compose application 

I've made a simple Music player app with the new Androidx Media3 library (never used any of media1 or media2).
I didn't find a pretty way to include the PlayerView in compose so i've made the ui with the controls (which was the fun part).
ExoPlayer is handeled in the PlaybackServiceHandler, leaving the ViewModel of any Media3 library existance.
ViewModel is getting the data from the repository updating it back and forth and recieving updates.
Values formating should have been in a core module, keeping the ui clean by accessing it with a LocalComposition.

As a side note : 
Updated all gradle files to gradle kotlin files with gradle catalog.
The Back button cause the player to never start again, I didn't looked at it.

#### Modules
- App : Basic application module, holding the application and the main activity. The app also resolves it's features Injections in the di package.
- Feature:music-player : Main feature with compose screen / viewmodel on one side and Playback service / handler on the other.
- Core:model : Shared models
- Core:ui : Compose ui and designsystem
- Core:data : Repository data module with network and cache data sources (they also could have been in their own modules but it was starting to be a bit too much JavaEnterpriseEdition)


## Time spent :
- Thusday : 1h
- Friday : 4h
- Saturday : 7h

## Captures : 
<img src="captures/Screenshot_20221210_160432.png" alt="drawing" width="200"/>
<img src="captures/Screenshot_20221210_160407.png" alt="drawing" width="200"/>

# Instructions :

This is a bootstrap of Exoplayer powered application, only able to load an url and play it for now.

The objective is to implement a basic play queue management on your own. Do not use ExoPlayer playlist API.
App should allow to:

- [x] Display the current media queue, alongside the player view
- [x] Chain playback from a media to the next one in the list
- [x] Start playback of any media in the queue on click on this media
- [x] Add or remove a media from this queue

Media can be local media (on device), embedded in assets or urls to stream from.

The goal of this exercise is to implement the player and queue list modules how you prefer, with the framework you want (Androidx Viewmodel is used to bootstrap the app here, keep it or replace it, it doesn’t matter for this exercice).
UI appearance and UI robustness are not important, we are focusing on the under layers and how they are exposed and used.

We are not expecting any particular architectural pattern or framework to use. This will be a basis for the next interview meeting.
