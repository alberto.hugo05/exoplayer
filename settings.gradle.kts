pluginManagement {
    repositories {
        // Repos are declared roughly in order of likely to hit.
        gradlePluginPortal()
        google()
    }
}

dependencyResolutionManagement {
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
    repositories {
        // Repos are declared roughly in order of likely to hit.
        google()
        mavenCentral()
    }
}

include(":app")
include(":core:data")
include(":core:model")
include(":core:ui")
include(":feature:music-player")

// https://docs.gradle.org/current/userguide/declaring_dependencies.html#sec:type-safe-project-accessors
enableFeaturePreview("TYPESAFE_PROJECT_ACCESSORS")

rootProject.name = "Exo-Application"