package com.deezer.exoapplication.core.model

@JvmInline
public value class StreamingUri(public val uri: String)

public data class Music(
    val title: String,
    val artist: String,
    val stream: StreamingUri
)
