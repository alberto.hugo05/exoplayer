plugins {
    id("com.android.library")
    kotlin("android")
    id("org.jmailen.kotlinter")
}

android {
    namespace = "com.deezer.exoapplication.core.model"
    compileSdk = 33

    defaultConfig {
        minSdk = 21
        targetSdk = 33
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_11
        targetCompatibility = JavaVersion.VERSION_11
    }

    kotlinOptions {
        // Treat all Kotlin warnings as errors (disabled by default)
        allWarningsAsErrors = true
        freeCompilerArgs = freeCompilerArgs + "-Xexplicit-api=strict"
        // Set JVM target to 11
        jvmTarget = JavaVersion.VERSION_11.toString()
    }
}
