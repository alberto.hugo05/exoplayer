plugins {
    id("com.android.library")
    kotlin("android")
    id("org.jmailen.kotlinter")
}

android {
    namespace = "com.deezer.exoapplication.core.data"
    compileSdk = 33

    defaultConfig {
        minSdk = 21
        targetSdk = 33
    }

    testOptions {
        unitTests.all {
            it.useJUnitPlatform()
        }
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_11
        targetCompatibility = JavaVersion.VERSION_11
    }

    kotlinOptions {
        // Treat all Kotlin warnings as errors (disabled by default)
        allWarningsAsErrors = true
        freeCompilerArgs = freeCompilerArgs + "-Xexplicit-api=strict"
        // Set JVM target to 11
        jvmTarget = JavaVersion.VERSION_11.toString()
    }
}

tasks.withType<Test>().configureEach {
    useJUnitPlatform()
}

dependencies {
    implementation(projects.core.model)
    implementation(libs.hilt.android)
    testImplementation(libs.kotest.runner.junit5)
    testImplementation(libs.kotest.assertions.core)
    testImplementation(libs.mockk.android)
    testImplementation(libs.mockk.agent)
    testImplementation(libs.turbine)
}