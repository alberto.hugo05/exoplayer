package com.deezer.exoapplication.core.data

import com.deezer.exoapplication.core.model.Music
import javax.inject.Inject

public interface PlaylistLocalDataSource {
    public val playlist: List<Music>
    public suspend fun savePlaylist(playlist: List<Music>)
}

public class DefaultPlaylistLocalDataSource @Inject constructor() : PlaylistLocalDataSource {

    private var _localMemoryPlaylist: MutableList<Music> = mutableListOf()
    public override val playlist: List<Music>
        get() = _localMemoryPlaylist

    override suspend fun savePlaylist(playlist: List<Music>) {
        _localMemoryPlaylist.clear()
        _localMemoryPlaylist.addAll(playlist)
    }
}
