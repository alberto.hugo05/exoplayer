package com.deezer.exoapplication.core.data

import com.deezer.exoapplication.core.model.Music
import com.deezer.exoapplication.core.model.StreamingUri
import javax.inject.Inject

public interface PlaylistRemoteDataSource {
    public suspend fun trendingPlaylist(): List<Music>
}

public class DefaultPlaylistRemoteDataSource @Inject constructor() : PlaylistRemoteDataSource {
    override suspend fun trendingPlaylist(): List<Music> {
        val playlist = mutableListOf<Music>()
        repeat(4) {
            val index = it + 1
            playlist.add(
                Music(
                    title = "Song $index",
                    artist = "Unknown Artist",
                    stream = StreamingUri(
                        uri = "https://filesamples.com/samples/audio/mp3/sample$index.mp3"
                    )
                )
            )
        }
        return playlist.toList()
    }
}
