package com.deezer.exoapplication.core.data

import androidx.annotation.VisibleForTesting
import com.deezer.exoapplication.core.model.Music
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.update
import javax.inject.Inject

public interface PlaylistRepository {
    public val trendingPlaylist: Flow<List<Music>>
    public suspend fun fetchTrendingPlaylist()
    public suspend fun deleteMusic(music: Music)
    public suspend fun addMusic()
}

public class DefaultPlaylistRepository @Inject constructor(
    private val remoteDataSource: PlaylistRemoteDataSource,
    private val localDataSource: PlaylistLocalDataSource
) : PlaylistRepository {

    private val _trendingPlaylist: MutableStateFlow<List<Music>> = MutableStateFlow(emptyList())
    public override val trendingPlaylist: Flow<List<Music>>
        get() = _trendingPlaylist

    override suspend fun fetchTrendingPlaylist() {
        val remoteData = remoteDataSource.trendingPlaylist()
        localDataSource.savePlaylist(remoteData)
        _trendingPlaylist.update { remoteData }
    }

    override suspend fun deleteMusic(music: Music) {
        _trendingPlaylist.update { it.toList() - music }
    }

    override suspend fun addMusic() {
        // Barbaric way to add missing musics based on the original fetched playlist
        val missingMusics = localDataSource.playlist - _trendingPlaylist.value.toSet()
        _trendingPlaylist.update { it.toList() + missingMusics.first() }
    }

    @VisibleForTesting
    public fun setPlaylist(playlist: List<Music>) {
        _trendingPlaylist.update { playlist }
    }
}
