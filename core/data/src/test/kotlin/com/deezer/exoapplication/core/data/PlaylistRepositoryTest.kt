package com.deezer.exoapplication.core.data

import app.cash.turbine.test
import com.deezer.exoapplication.core.model.Music
import com.deezer.exoapplication.core.model.StreamingUri
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe
import io.mockk.Runs
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.confirmVerified
import io.mockk.just
import io.mockk.mockk
import io.mockk.verify
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.update
import javax.inject.Inject

/**
 * Test class for [PlaylistRepository]
 */
public class PlaylistRepositoryTest : StringSpec({

    lateinit var repository: DefaultPlaylistRepository
    lateinit var remoteDataSource: PlaylistRemoteDataSource
    lateinit var localDataSource: PlaylistLocalDataSource

    beforeTest {
        remoteDataSource = mockk()
        localDataSource = mockk()
        repository = DefaultPlaylistRepository(
            remoteDataSource = remoteDataSource,
            localDataSource = localDataSource
        )
    }

    "Fetching the trending playlist should trigger playlist's flow" {
        val expected = listOf(
            Music(
                title = "title 1",
                artist = "artist 1",
                stream = StreamingUri(uri = "//uri:1")
            ),
            Music(
                title = "title 2",
                artist = "artist 2",
                stream = StreamingUri(uri = "//uri:2")
            )
        )

        coEvery { remoteDataSource.trendingPlaylist() } returns expected
        coEvery { localDataSource.savePlaylist(any()) } just Runs

        repository.fetchTrendingPlaylist()

        repository.trendingPlaylist.test {
            awaitItem() shouldBe expected
        }

        coVerify { remoteDataSource.trendingPlaylist() }
        coVerify { localDataSource.savePlaylist(any()) }

        confirmVerified(remoteDataSource, localDataSource)
    }

    "Deleting a music in the playlist should trigger playlist's flow without it" {
        val expected = listOf(
            Music(
                title = "title 1",
                artist = "artist 1",
                stream = StreamingUri(uri = "//uri:1")
            ),
            Music(
                title = "title 2",
                artist = "artist 2",
                stream = StreamingUri(uri = "//uri:2")
            )
        )

        repository.setPlaylist(expected)
        repository.deleteMusic(expected[0])

        repository.trendingPlaylist.test {
            awaitItem() shouldBe (expected - expected[0])
        }
    }
})