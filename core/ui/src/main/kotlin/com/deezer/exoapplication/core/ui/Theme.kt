package com.deezer.exoapplication.core.ui

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.darkColorScheme
import androidx.compose.material3.lightColorScheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.ui.graphics.Color

internal val lightScheme = lightColorScheme(background = Color.White)
internal val darkScheme = darkColorScheme(background = Color.Black)

@Composable
public fun ExoTheme(
    spacings: Spacings = Spacings(),
    darkTheme: Boolean = isSystemInDarkTheme(),
    content: @Composable () -> Unit
) {
    val colorScheme = if (darkTheme) darkScheme else lightScheme

    CompositionLocalProvider(LocalSpacings provides spacings) {
        MaterialTheme(
            colorScheme = colorScheme,
            typography = ExoTypography,
            content = content
        )
    }
}
