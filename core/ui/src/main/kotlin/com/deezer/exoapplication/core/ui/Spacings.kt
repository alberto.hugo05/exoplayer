package com.deezer.exoapplication.core.ui

import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.Immutable
import androidx.compose.runtime.ProvidableCompositionLocal
import androidx.compose.runtime.ReadOnlyComposable
import androidx.compose.runtime.staticCompositionLocalOf
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp

@Immutable
public class Spacings(
    public val extraSmall: Dp = SpacingsDefaults.ExtraSmall,
    public val small: Dp = SpacingsDefaults.Small,
    public val medium: Dp = SpacingsDefaults.Medium,
    public val large: Dp = SpacingsDefaults.Large,
    public val extraLarge: Dp = SpacingsDefaults.ExtraLarge
)

@Suppress("unused")
public val MaterialTheme.spacings: Spacings
    @Composable
    @ReadOnlyComposable
    get() = LocalSpacings.current

public object SpacingsDefaults {
    public val ExtraSmall: Dp = 8.dp
    public val Small: Dp = 12.dp
    public val Medium: Dp = 16.dp
    public val Large: Dp = 24.dp
    public val ExtraLarge: Dp = 30.dp
}

public val LocalSpacings: ProvidableCompositionLocal<Spacings> = staticCompositionLocalOf {
    Spacings()
}
