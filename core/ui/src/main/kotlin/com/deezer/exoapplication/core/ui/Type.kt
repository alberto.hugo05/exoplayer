package com.deezer.exoapplication.core.ui

import androidx.compose.material3.Typography

internal val ExoTypography = Typography()
