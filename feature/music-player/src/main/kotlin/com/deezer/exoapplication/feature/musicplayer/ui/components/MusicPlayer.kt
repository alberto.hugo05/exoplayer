package com.deezer.exoapplication.feature.musicplayer.ui.components

import android.content.res.Configuration
import androidx.annotation.OptIn
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.slideInVertically
import androidx.compose.animation.slideOutVertically
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.navigationBarsPadding
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.ZeroCornerSize
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Pause
import androidx.compose.material.icons.filled.PlayArrow
import androidx.compose.material.icons.filled.SkipNext
import androidx.compose.material.icons.filled.SkipPrevious
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.ElevatedCard
import androidx.compose.material3.FilledIconButton
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Slider
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.media3.common.util.UnstableApi
import com.deezer.exoapplication.core.ui.ExoTheme
import com.deezer.exoapplication.core.ui.spacings
import com.deezer.exoapplication.feature.musicplayer.R
import com.deezer.exoapplication.feature.musicplayer.ui.PlayerState
import java.lang.String.format
import java.util.*

@Composable
@OptIn(UnstableApi::class)
internal fun MusicPlayer(
    playerState: () -> PlayerState,
    playerProgressState: () -> PlayerState.Progress,
    onPlay: () -> Unit,
    onPrev: () -> Unit,
    onNext: () -> Unit,
    onProgressUpdate: (Float) -> Unit
) {
    val state = playerState()
    AnimatedVisibility(
        visible = state.mediaReady,
        enter = slideInVertically { it },
        exit = slideOutVertically { it }
    ) {
        ElevatedCard(
            elevation = CardDefaults.elevatedCardElevation(defaultElevation = 5.dp),
            shape = MaterialTheme.shapes.large.copy(
                bottomEnd = ZeroCornerSize,
                bottomStart = ZeroCornerSize
            ),
            modifier = Modifier.fillMaxWidth()
        ) {
            Column(
                modifier = Modifier
                    .padding(MaterialTheme.spacings.large)
                    .navigationBarsPadding()
            ) {
                Row(verticalAlignment = Alignment.CenterVertically) {
                    Column {
                        Text(text = stringResource(R.string.player_screen_player_title))
                        Text(
                            text = state.currentMusic?.title
                                ?: stringResource(R.string.player_screen_player_music_unknown)
                        )
                        Text(
                            text = state.currentMusic?.artist ?: "",
                            style = MaterialTheme.typography.bodySmall
                        )
                    }
                    Spacer(modifier = Modifier.weight(1f))
                    PreviousButton(enabled = state.canPrevious, onPrev = onPrev)
                    PlayPauseButton(isPlaying = state.isPlaying, onPlay = onPlay)
                    NextButton(enabled = state.canNext, onNext = onNext)
                }
                ProgressBar(
                    state = playerProgressState,
                    onProgress = onProgressUpdate
                )
            }
        }
    }
}

@Composable
private fun PlayPauseButton(isPlaying: Boolean, onPlay: () -> Unit) {
    FilledIconButton(onClick = onPlay) {
        Icon(
            imageVector = if (isPlaying) Icons.Filled.Pause else Icons.Filled.PlayArrow,
            contentDescription = stringResource(
                id = if (isPlaying) {
                    R.string.player_screen_play_action
                } else {
                    R.string.player_screen_pause_action
                }
            )
        )
    }
}

@Composable
private fun NextButton(enabled: Boolean, onNext: () -> Unit) {
    IconButton(enabled = enabled, onClick = onNext) {
        Icon(
            imageVector = Icons.Filled.SkipNext,
            contentDescription = stringResource(id = R.string.player_screen_next_action)
        )
    }
}

@Composable
private fun PreviousButton(enabled: Boolean, onPrev: () -> Unit) {
    IconButton(enabled = enabled, onClick = onPrev) {
        Icon(
            imageVector = Icons.Filled.SkipPrevious,
            contentDescription = stringResource(id = R.string.player_screen_previous_action)
        )
    }
}

@Composable
private fun ProgressBar(
    state: () -> PlayerState.Progress,
    onProgress: (Float) -> Unit
) {
    Column {
        // Split views to have as little recompositions as possible
        ProgressSlider(state = state, onProgress = onProgress)
        ProgressTimers(state = state)
    }
}

@Composable
private fun ProgressSlider(state: () -> PlayerState.Progress, onProgress: (Float) -> Unit) {
    val uiProgress = remember { mutableStateOf(0f) }
    val (sliding, setSliding) = remember { mutableStateOf(false) }
    Slider(
        value = if (sliding) uiProgress.value else state().progress ?: 0f,
        onValueChange = { newValue ->
            uiProgress.value = newValue
            setSliding(true)
            onProgress(newValue)
        },
        onValueChangeFinished = { setSliding(false) }
    )
}

@Composable
private fun ProgressTimers(state: () -> PlayerState.Progress) {
    Row(
        horizontalArrangement = Arrangement.SpaceBetween,
        modifier = Modifier.fillMaxWidth()
    ) {
        @Suppress("NAME_SHADOWING")
        val state = state()
        val progressStr by remember(state.progress) {
            derivedStateOf {
                milliToReadable(duration = ((state.duration ?: 0L) * (state.progress ?: 0f)).toLong())
            }
        }
        Text(text = progressStr)
        val durationStr by remember(state.duration) {
            derivedStateOf {
                milliToReadable(duration = state.duration)
            }
        }
        Text(text = durationStr)
    }
}

private fun milliToReadable(duration: Long?): String = if (duration != null) {
    val minutes = duration / 1000 / 60
    val seconds = duration / 1000 % 60
    format(Locale.FRENCH, "%02d:%02d", minutes, seconds)
} else {
    ""
}

@Preview
@Preview(uiMode = Configuration.UI_MODE_NIGHT_YES)
@Composable
private fun MusicPlayerPreview() {
    ExoTheme {
        Surface(color = MaterialTheme.colorScheme.background) {
            MusicPlayer(
                playerState = { PlayerState(mediaReady = true) },
                playerProgressState = { PlayerState.Progress() },
                onPrev = {},
                onPlay = {},
                onNext = {},
                onProgressUpdate = {}
            )
        }
    }
}
