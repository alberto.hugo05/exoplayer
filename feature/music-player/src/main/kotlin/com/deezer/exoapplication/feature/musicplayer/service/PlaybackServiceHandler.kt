package com.deezer.exoapplication.feature.musicplayer.service

import androidx.media3.common.MediaItem
import androidx.media3.common.Player
import androidx.media3.exoplayer.ExoPlayer
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

public class PlaybackServiceHandler @Inject constructor(
    private val exoPlayer: ExoPlayer
) : Player.Listener {

    private var _job: Job? = null
    private lateinit var _scope: CoroutineScope

    private val _state = MutableStateFlow<MediaState>(MediaState.Initial)
    public val state: StateFlow<MediaState> = _state.asStateFlow()

    init {
        exoPlayer.addListener(this)
    }

    override fun onPlaybackStateChanged(playbackState: Int): Unit = when (playbackState) {
        // Keep up the ui with the buffering going on
        ExoPlayer.STATE_BUFFERING -> _state.update { MediaState.Buffering(exoPlayer.currentPosition) }
        ExoPlayer.STATE_READY -> {
            _state.update { MediaState.Ready(exoPlayer.duration) }
            exoPlayer.play()
        }
        else -> { /*no-op */ }
    }

    override fun onIsPlayingChanged(isPlaying: Boolean) {
        _state.update { MediaState.Playing(isPlaying = isPlaying) }
        if (isPlaying) {
            _scope.launch(Dispatchers.Main) {
                startProgressUpdate()
            }
        } else {
            stopProgressUpdate()
        }
    }

    public fun setLifecycle(scope: CoroutineScope) {
        _scope = scope
    }

    public fun stop() {
        stopProgressUpdate()
        exoPlayer.release()
    }

    public suspend fun addMediaItem(mediaItem: MediaItem): Unit = withContext(Dispatchers.Main) {
        exoPlayer.setMediaItem(mediaItem)
        exoPlayer.prepare()
    }

    public suspend fun play(): Unit = withContext(Dispatchers.Main) {
        exoPlayer.play()
        startProgressUpdate()
    }

    public suspend fun pause(): Unit = withContext(Dispatchers.Main) {
        exoPlayer.pause()
        stopProgressUpdate()
    }

    public suspend fun back(): Unit = withContext(Dispatchers.Main) {
        // Normally you would :
        // exoPlayer.seekBack()
    }

    public suspend fun forward(): Unit = withContext(Dispatchers.Main) {
        // Normally you would :
        // exoPlayer.seekForward()
    }

    public suspend fun progress(progress: Float): Unit = withContext(Dispatchers.Main) {
        exoPlayer.seekTo((exoPlayer.duration * progress).toLong())
    }

    private suspend fun startProgressUpdate() {
        _job = Job().also {
            while (it.isActive) {
                delay(1000)
                _state.update {
                    MediaState.Progress(progress = exoPlayer.currentPosition)
                }
            }
        }
    }

    private fun stopProgressUpdate() {
        _job?.cancel()
    }
}

public sealed interface MediaState {
    public object Initial : MediaState
    public data class Ready(val duration: Long) : MediaState
    public data class Buffering(val progress: Long) : MediaState
    public data class Progress(val progress: Long) : MediaState
    public data class Playing(
        val progress: Long = 0,
        val isPlaying: Boolean = false
    ) : MediaState
}
