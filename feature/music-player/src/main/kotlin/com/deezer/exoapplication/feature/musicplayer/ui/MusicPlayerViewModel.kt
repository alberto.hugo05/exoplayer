package com.deezer.exoapplication.feature.musicplayer.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.media3.common.MediaItem
import androidx.media3.common.MediaMetadata
import com.deezer.exoapplication.core.data.PlaylistRepository
import com.deezer.exoapplication.core.model.Music
import com.deezer.exoapplication.feature.musicplayer.service.MediaState.Buffering
import com.deezer.exoapplication.feature.musicplayer.service.MediaState.Initial
import com.deezer.exoapplication.feature.musicplayer.service.MediaState.Playing
import com.deezer.exoapplication.feature.musicplayer.service.MediaState.Progress
import com.deezer.exoapplication.feature.musicplayer.service.MediaState.Ready
import com.deezer.exoapplication.feature.musicplayer.service.PlaybackServiceHandler
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.collections.immutable.ImmutableList
import kotlinx.collections.immutable.persistentListOf
import kotlinx.collections.immutable.toImmutableList
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted.Companion.WhileSubscribed
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.annotation.concurrent.Immutable
import javax.inject.Inject

@HiltViewModel
public class MusicPlayerViewModel @Inject constructor(
    private val playbackServiceHandler: PlaybackServiceHandler,
    private val playlistRepository: PlaylistRepository
) : ViewModel() {

    internal val state = playlistRepository.trendingPlaylist
        .onEach { playlist ->
            // Keep a cached version
            _cachedPlaylist = playlist.toImmutableList()
            _playerState.update {
                it.copy(
                    canPrevious = _playingIndex > 0,
                    canNext = _playingIndex < _cachedPlaylist.size - 1
                )
            }
        }.map {
            HomeState(
                playlist = it.toImmutableList(),
                canAddMusics = it.size < 4 // meh
            )
        }.stateIn(
            scope = viewModelScope,
            started = WhileSubscribed(5_000),
            initialValue = HomeState()
        )

    // Player state will often be updated, split it from the main view state
    private val _playerState = MutableStateFlow(PlayerState())
    internal val playerState: StateFlow<PlayerState> = _playerState.asStateFlow()

    private var _cachedPlaylist: ImmutableList<Music> = persistentListOf()
    private var _playingIndex: Int = -1

    init {
        playbackServiceHandler.setLifecycle(viewModelScope)
        listenServiceState()
        viewModelScope.launch(Dispatchers.IO) {
            fetchPlaylist()
        }
    }

    override fun onCleared() {
        super.onCleared()
        playbackServiceHandler.stop()
    }

    public fun onPlayMusic(music: Music) {
        viewModelScope.launch(Dispatchers.IO) {
            setMusic(music)
        }
    }

    public fun onPlay() {
        viewModelScope.launch(Dispatchers.IO) {
            if (playerState.value.isPlaying) {
                playbackServiceHandler.pause()
            } else {
                playbackServiceHandler.play()
            }
        }
    }

    public fun onPrev() {
        viewModelScope.launch(Dispatchers.IO) {
            val previousMusic = _cachedPlaylist[_playingIndex - 1]
            setMusic(previousMusic)
        }
    }

    public fun onNext() {
        viewModelScope.launch(Dispatchers.IO) {
            val nextMusic = _cachedPlaylist[_playingIndex + 1]
            setMusic(nextMusic)
        }
    }

    public fun onProgressUpdate(progress: Float) {
        viewModelScope.launch(Dispatchers.IO) {
            playbackServiceHandler.progress(progress)
            _playerState.update { it.copy(progress = it.progress.copy(progress = progress)) }
        }
    }

    public fun onAddMusic() {
        viewModelScope.launch(Dispatchers.IO) {
            playlistRepository.addMusic()
        }
    }

    public fun onDeleteMusic(music: Music) {
        viewModelScope.launch(Dispatchers.IO) {
            val musicIndex = _cachedPlaylist.indexOf(music)
            if (musicIndex == _playingIndex) {
                playbackServiceHandler.pause()
                _playerState.update { it.copy(currentMusic = null, mediaReady = false) }
            } else if (musicIndex < _playingIndex) {
                _playingIndex--
            }
            playlistRepository.deleteMusic(music)
        }
    }

    private fun listenServiceState() = playbackServiceHandler.state.onEach { mediaState ->
        when (mediaState) {
            Initial -> {}
            is Playing -> _playerState.update {
                it.copy(mediaReady = true, isPlaying = mediaState.isPlaying)
            }

            is Ready -> _playerState.update {
                it.copy(
                    mediaReady = true,
                    progress = it.progress.copy(duration = mediaState.duration)
                )
            }

            is Buffering -> updateProgress(mediaState.progress)
            is Progress -> updateProgress(mediaState.progress)
        }
    }.flowOn(Dispatchers.IO).launchIn(viewModelScope)

    private fun updateProgress(progress: Long) {
        val progressResult = kotlin.runCatching {
            if (progress > 0) {
                (progress.toFloat() / playerState.value.progress.duration!!)
            } else {
                0f
            }
        }
        progressResult.getOrNull()?.let { res ->
            _playerState.update { it.copy(progress = it.progress.copy(progress = res)) }
        }
    }

    private suspend fun fetchPlaylist() {
        playlistRepository.fetchTrendingPlaylist()
    }

    private suspend fun setMusic(music: Music) {
        val mediaItem = MediaItem.Builder()
            .setUri(music.stream.uri)
            .setMediaMetadata(
                MediaMetadata.Builder()
                    .setAlbumArtist(music.artist)
                    .setDisplayTitle(music.title)
                    .build()
            ).build()
        playbackServiceHandler.addMediaItem(mediaItem)
        _playingIndex = _cachedPlaylist.indexOf(music)
        _playerState.update {
            it.copy(
                currentMusic = music,
                canPrevious = _playingIndex > 0,
                canNext = _playingIndex < _cachedPlaylist.size - 1
            )
        }
    }
}

@Immutable
internal data class HomeState(
    val playlist: ImmutableList<Music> = persistentListOf(),
    val canAddMusics: Boolean = false
)

@Immutable
internal data class PlayerState(
    val mediaReady: Boolean = false,
    val isPlaying: Boolean = false,
    val canPrevious: Boolean = true,
    val canNext: Boolean = true,
    val currentMusic: Music? = null,
    val progress: Progress = Progress()
) {

    @Immutable
    internal data class Progress(
        val duration: Long? = null,
        val progress: Float? = null
    )
}
