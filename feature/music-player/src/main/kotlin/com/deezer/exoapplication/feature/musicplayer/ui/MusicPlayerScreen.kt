package com.deezer.exoapplication.feature.musicplayer.ui

import android.content.Intent
import android.content.res.Configuration
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.navigationBarsPadding
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.statusBarsPadding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material.icons.filled.PlayArrow
import androidx.compose.material3.Button
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.rememberUpdatedState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Alignment.Companion.CenterHorizontally
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.tooling.preview.PreviewParameter
import androidx.compose.ui.tooling.preview.PreviewParameterProvider
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import androidx.lifecycle.compose.ExperimentalLifecycleComposeApi
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import com.deezer.exoapplication.core.model.Music
import com.deezer.exoapplication.core.model.StreamingUri
import com.deezer.exoapplication.core.ui.ExoTheme
import com.deezer.exoapplication.core.ui.spacings
import com.deezer.exoapplication.feature.musicplayer.R
import com.deezer.exoapplication.feature.musicplayer.R.string.player_screen_add_music_action
import com.deezer.exoapplication.feature.musicplayer.R.string.player_screen_current_playlist_title
import com.deezer.exoapplication.feature.musicplayer.R.string.player_screen_title
import com.deezer.exoapplication.feature.musicplayer.service.PlaybackService
import com.deezer.exoapplication.feature.musicplayer.ui.components.MusicPlayer
import kotlinx.collections.immutable.ImmutableList
import kotlinx.collections.immutable.persistentListOf

@OptIn(ExperimentalLifecycleComposeApi::class)
@Composable
public fun MusicPlayerRoute(
    viewModel: MusicPlayerViewModel = hiltViewModel(),
    startService: (Intent) -> Unit,
    stopService: (Intent) -> Unit
) {
    val state by viewModel.state.collectAsStateWithLifecycle()
    val playerState by viewModel.playerState.collectAsStateWithLifecycle()

    val lifecycleOwner = rememberUpdatedState(LocalLifecycleOwner.current)
    val context = LocalContext.current
    DisposableEffect(lifecycleOwner.value) {
        val lifecycle = lifecycleOwner.value.lifecycle
        val observer = LifecycleEventObserver { _, event ->
            when (event) {
                Lifecycle.Event.ON_START ->
                    startService(Intent(context, PlaybackService::class.java))

                Lifecycle.Event.ON_STOP ->
                    stopService(Intent(context, PlaybackService::class.java))

                else -> { /* no-op */
                }
            }
        }

        lifecycle.addObserver(observer)
        onDispose {
            lifecycle.removeObserver(observer)
        }
    }

    MusicPlayerScreen(
        state = state,
        playerState = { playerState },
        playerProgressState = { playerState.progress },
        onPlayMusic = viewModel::onPlayMusic,
        onAddMusic = viewModel::onAddMusic,
        onDeleteMusic = viewModel::onDeleteMusic,
        onPlay = viewModel::onPlay,
        onPrev = viewModel::onPrev,
        onNext = viewModel::onNext,
        onProgressUpdate = viewModel::onProgressUpdate
    )
}

@Composable
private fun MusicPlayerScreen(
    state: HomeState,
    playerState: () -> PlayerState,
    playerProgressState: () -> PlayerState.Progress,
    onPlayMusic: (Music) -> Unit,
    onAddMusic: () -> Unit,
    onDeleteMusic: (Music) -> Unit,
    onPlay: () -> Unit,
    onPrev: () -> Unit,
    onNext: () -> Unit,
    onProgressUpdate: (Float) -> Unit
) {
    Column(
        verticalArrangement = Arrangement.spacedBy(MaterialTheme.spacings.large),
        modifier = Modifier.statusBarsPadding()
    ) {
        Text(
            text = stringResource(player_screen_title),
            style = MaterialTheme.typography.headlineLarge,
            modifier = Modifier.padding(MaterialTheme.spacings.extraLarge)
        )
        Playlist(
            playlist = state.playlist,
            onPlayMusic = onPlayMusic,
            onDeleteMusic = onDeleteMusic,
            modifier = Modifier
                .weight(1f)
                .fillMaxWidth()
                .padding(horizontal = MaterialTheme.spacings.extraLarge)
        )
        AddMusicButton(
            enabled = state.canAddMusics,
            onAddMusic = onAddMusic,
            modifier = Modifier
                .align(CenterHorizontally)
                .navigationBarsPadding()
        )
        MusicPlayer(
            playerState = playerState,
            playerProgressState = playerProgressState,
            onPlay = onPlay,
            onPrev = onPrev,
            onNext = onNext,
            onProgressUpdate = onProgressUpdate
        )
    }
}

@OptIn(ExperimentalFoundationApi::class)
@Composable
private fun Playlist(
    playlist: ImmutableList<Music>,
    onPlayMusic: (Music) -> Unit,
    onDeleteMusic: (Music) -> Unit,
    modifier: Modifier = Modifier
) {
    LazyColumn(modifier = modifier) {
        stickyHeader {
            Text(
                text = stringResource(player_screen_current_playlist_title),
                style = MaterialTheme.typography.titleMedium
            )
        }
        items(playlist) {
            Music(
                music = it,
                onPlayMusic = onPlayMusic,
                onDeleteMusic = onDeleteMusic
            )
        }
    }
}

@Composable
private fun Music(
    music: Music,
    onPlayMusic: (Music) -> Unit,
    onDeleteMusic: (Music) -> Unit
) {
    Row(
        horizontalArrangement = Arrangement.spacedBy(MaterialTheme.spacings.large),
        verticalAlignment = Alignment.CenterVertically
    ) {
        IconButton(onClick = { onPlayMusic(music) }) {
            Icon(
                imageVector = Icons.Filled.PlayArrow,
                contentDescription = stringResource(R.string.player_screen_play_action)
            )
        }
        Column {
            Text(text = music.title)
            Text(text = music.artist, style = MaterialTheme.typography.bodySmall)
        }
        Spacer(modifier = Modifier.weight(1f))
        IconButton(onClick = { onDeleteMusic(music) }) {
            Icon(
                imageVector = Icons.Filled.Delete,
                tint = MaterialTheme.colorScheme.error,
                contentDescription = stringResource(id = R.string.player_screen_delete_action)
            )
        }
    }
}

@Composable
private fun AddMusicButton(
    enabled: Boolean,
    onAddMusic: () -> Unit,
    modifier: Modifier = Modifier
) {
    Button(modifier = modifier, enabled = enabled, onClick = onAddMusic) {
        Text(text = stringResource(player_screen_add_music_action))
    }
}

@Preview
@Preview(uiMode = Configuration.UI_MODE_NIGHT_YES)
@Composable
private fun MusicPlayerScreenPreview(
    @PreviewParameter(MusicPlayerScreenProvider::class) state: HomeState
) {
    ExoTheme {
        Surface(color = MaterialTheme.colorScheme.background) {
            MusicPlayerScreen(
                state = state,
                playerState = { PlayerState(mediaReady = true) },
                playerProgressState = { PlayerState.Progress() },
                onNext = {},
                onAddMusic = {},
                onDeleteMusic = {},
                onPlayMusic = {},
                onPlay = {},
                onPrev = {},
                onProgressUpdate = {}
            )
        }
    }
}

private class MusicPlayerScreenProvider : PreviewParameterProvider<HomeState> {
    val musics = persistentListOf(
        Music(
            title = "Title",
            artist = "Artist",
            stream = StreamingUri(uri = "//uri")
        ),
        Music(
            title = "Title 2",
            artist = "Artist",
            stream = StreamingUri(uri = "//uri")
        ),
        Music(
            title = "Title 3",
            artist = "Artist",
            stream = StreamingUri(uri = "//uri")
        )
    )
    override val values = sequenceOf(
        HomeState(playlist = musics)
    )
}
