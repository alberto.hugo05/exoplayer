package com.deezer.exoapplication.di

import com.deezer.exoapplication.core.data.DefaultPlaylistLocalDataSource
import com.deezer.exoapplication.core.data.DefaultPlaylistRemoteDataSource
import com.deezer.exoapplication.core.data.DefaultPlaylistRepository
import com.deezer.exoapplication.core.data.PlaylistLocalDataSource
import com.deezer.exoapplication.core.data.PlaylistRemoteDataSource
import com.deezer.exoapplication.core.data.PlaylistRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent

@Module
@InstallIn(ViewModelComponent::class)
internal abstract class DataModule {

    @Binds
    abstract fun bindPlaylistRepository(
        playlistRepository: DefaultPlaylistRepository
    ): PlaylistRepository

    @Binds
    abstract fun bindPlaylistRemoteDataSource(
        playlistRemoteDataSource: DefaultPlaylistRemoteDataSource
    ): PlaylistRemoteDataSource

    @Binds
    abstract fun bindPlaylistLocalDataSource(
        playlistLocalDataSource: DefaultPlaylistLocalDataSource
    ): PlaylistLocalDataSource
}
