package com.deezer.exoapplication.ui

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.DisposableEffect
import androidx.compose.ui.graphics.Color
import androidx.core.view.WindowCompat
import com.deezer.exoapplication.core.ui.ExoTheme
import com.deezer.exoapplication.feature.musicplayer.ui.MusicPlayerRoute
import com.google.accompanist.systemuicontroller.rememberSystemUiController
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Tell Android to not add any window padding
        WindowCompat.setDecorFitsSystemWindows(window, false)

        setContent {
            val systemUiController = rememberSystemUiController()
            val isInDarkTheme = isSystemInDarkTheme()
            // Update the dark content of the system bars to match the theme
            DisposableEffect(systemUiController, isInDarkTheme) {
                systemUiController.setNavigationBarColor(
                    color = Color.Transparent,
                    darkIcons = !isInDarkTheme
                )
                onDispose {}
            }

            ExoTheme {
                Surface(color = MaterialTheme.colorScheme.background) {
                    MusicPlayerRoute(
                        startService = { startService(it) },
                        stopService = { stopService(it) }
                    )
                }
            }
        }
    }
}
