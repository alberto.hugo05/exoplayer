plugins {
    alias(libs.plugins.android.library) apply false
    alias(libs.plugins.kotlin.android) apply false
    alias(libs.plugins.kotlinx.kover)
    alias(libs.plugins.kotest) apply false
    alias(libs.plugins.hilt.android) apply false
    alias(libs.plugins.jmailen.kotlinter)
}